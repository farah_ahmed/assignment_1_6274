import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import static java.util.Arrays.sort;

public class class2 extends Application {
    Stage stage;
    Scene scene1,scene2;
    Button button1=new Button("add activity");
    Button button2=new Button("show results");
    Button button3=new Button("close");
    TextField text =new TextField("time");
     Label l1 =new Label();
     Label l2=new Label();
     Label l3=new Label();
     Label l4=new Label();
    Label rank=new Label("Activities Rank");
    Label one =new Label();
    Label two=new Label();
    Label three =new Label();
    Label four=new Label();
    Label five=new Label();
    Label six=new Label();
    Label seven=new Label();
    Label eight=new Label();
    Label nine=new Label();
    Label ten=new Label();
    Label eleven=new Label();
    Label twelve=new Label();
     Label therteen=new Label();
     Label fourteen=new Label();
     Label space1=new Label("           ");
     Label space2 =new Label("           ");
     Label space3 =new Label("           ");
     Label space4=new Label("           ");
     Label space5 =  new Label("           ");
     Label title=new Label("ADD ACTIVITY");
        public static void main (String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
       ChoiceBox<String> c=new ChoiceBox<>();
       c.getItems().addAll("SWIMMING","RUNNING","KICK BOXING","STRENGHT TRAINING");

        stage=primaryStage;
     stage.setTitle("simple fitness tracker");
        GridPane grid1=new GridPane();
        grid1.add(title,0,0);
      grid1.add(button1,0,3);
      grid1.add(button2,0,4);
      grid1.add(c,0,1);
      grid1.add(text,1,1);
      grid1.add(l1,10,10);
        grid1.add(l2,10,11);
        grid1.add(l3,10,12);
        grid1.add(l4,10,13);
        
      //add activity
      button1.setOnAction(event -> {int time=Integer.parseInt(text.getText());
          if (time != 0 &&( c.getValue()=="SWIMMING"  ||c.getValue()=="RUNNING"  || c.getValue()=="KICK BOXING" ||c.getValue()=="STRENGHT TRAINING"  )) {
              class3.display("messege", "Record added Successfuly!");
          }


     if( c.getValue()=="SWIMMING")
     {double a=class1.heart_rate;
     class1.heart_rate += class1.heart_rate * class1.swim_rate * time;
     class1.swimming_time += time;
     class1.total_swim_rate+=(class1.heart_rate-a);
     }
     if( c.getValue()=="RUNNING")
     { double a=class1.heart_rate;
         class1.heart_rate += class1.heart_rate * class1.run_rate * time;
         class1.running_time += time;
         class1.total_run_rate+=(class1.heart_rate-a);

     }
     if( c.getValue()=="KICK BOXING")
     {
         double a=class1.heart_rate;
         class1.heart_rate += class1.heart_rate * class1.box_rate * time;
         class1.boxing_time += time;
         class1.total_box_rate+=(class1.heart_rate-a);
     }
     if( c.getValue()=="STRENGHT TRAINING")
     {
         double a=class1.heart_rate;
         class1.heart_rate += class1.heart_rate * class1.strength_rate * time;
         class1.strenght_time+= time;
         class1.total_strength_rate+=(class1.heart_rate-a);
     }
     l1.setText("swimming time =" +class1.swimming_time);
          l2.setText("running time =" +class1.running_time);
          l3.setText("boxing time =" +class1.boxing_time);
          l4.setText("strenght time =" +class1.strenght_time);

      });
      //show results
      button2.setOnAction(event -> {
          stage.setScene(scene2);
          class1.total_swim_cal = class1.swimming_time * class1.swim_cal;
          class1.total_run_cal += class1.running_time * class1.run_cal;
          class1.total_box_cal += class1.boxing_time * class1.box_cal;
          class1.total_strenght_cal += class1.strenght_time * class1.strenght_cal;
          class1.calories = class1.total_swim_cal + class1.total_run_cal +class1.total_box_cal + class1.total_strenght_cal;

          int[] array1;
          array1 = new int[]{class1.total_swim_cal, class1.total_run_cal, class1.total_box_cal, class1.total_strenght_cal};
         double[] array2;
         array2 = new double[]{class1.total_swim_rate, class1.total_run_rate, class1.total_box_rate, class1.total_strength_rate};
        String[] print={"SWIMMING","RUNNING","KICK BOXING","STRENGHT TRAINING"};
          for (int i = 0; i < 3; i++)
              for (int j = 0; j < 3-i; j++)
                  if (array1[j] > array1[j+1]) {
                      int temp = array1[j];
                      array1[j] = array1[j+1];
                      array1[j+1] = temp;
                      double temp2 = array2[j];
                      array2[j] = array2[j+1];
                      array2[j+1] = temp2;

                      String temp3 = print[j];
                      print[j] = print[j+1];
                      print[j+1] = temp3;
                  }
          for (int i = 0; i < 3; i++) {
              if (array1[i] == array1[i+1]) {
                  if (array2[i] > array2[i + 1]) {
                      int temp = array1[i];
                      array1[i] = array1[i + 1];
                      array1[i + 1] = temp;
                      double temp2 = array2[i];
                      array2[i] = array2[i + 1];
                      array2[i + 1] = temp2;
                      String temp3 = print[i];
                      print[i] = print[i + 1];
                      print[i + 1] = temp3;
                  }
              }
          }
          one.setText(print[3]+":");
          one.setFont(new Font("Verdana",28));

          two.setText("calories burnt =" +array1[3]+" calories");
          two.setFont(new Font("Verdana",22));

          three.setText("heart rate increse=" +array2[3]+" beat/min");
         three.setFont(new Font("Verdana",22));

          four.setText(print[2]+":");
          four.setFont(new Font("Verdana",28));

          five.setText("calories burnt =" +array1[2]+" calories");
          five.setFont(new Font("Verdana",22));

          six.setText("heart rate increse=" +array2[2]+" beat/min");
          six.setFont(new Font("Verdana",22));

          seven.setText(print[1]+":");
          seven.setFont(new Font("Verdana",28));

          eight.setText("calories burnt =" +array1[1]+" calories");
          eight.setFont(new Font("Verdana",22));

          nine.setText("heart rate increse=" +array2[1]+" beat/min");
          nine.setFont(new Font("Verdana",22));

          ten.setText(print[0] + ":");
          ten.setFont(new Font("Verdana",28));

          eleven.setText("calories burnt =" +array1[0]+" calories");
          eleven.setFont(new Font("Verdana",22));

          twelve.setText("heart rate increse=" +array2[0]+" beat/min");
          twelve.setFont(new Font("Verdana",22));

          therteen.setText("TOTAL CALORIES="+ class1.calories+" calories");
          therteen.setFont(new Font("Verdana",30));

          fourteen.setText("HEART RATE="+class1.heart_rate +" beat/min");
          fourteen.setFont(new Font("Verdana",30));

          rank.setFont(new Font("Verdana",35));
      });


      scene1=new Scene(grid1,600,400);
        GridPane grid2=new GridPane();
        grid2.add(button3,0,20);
        grid2.add(space1,0,2);

        grid2.add(rank,0,3);
        grid2.add(space2,0,4);

        grid2.add(one,0,5);
        grid2.add(two,0,6);
        grid2.add(three,0,7);
        grid2.add(space3,0,8);

        grid2.add(four,0,9);
        grid2.add(five,0,10);
        grid2.add(six,0,11);
        grid2.add(space4,0,12);

        grid2.add(seven,0,13);
        grid2.add(eight,0,14);
        grid2.add(nine,0,15);
        grid2.add(space5,0,16);

        grid2.add(ten,0,17);
        grid2.add(eleven,0,18);
        grid2.add(twelve,0,19);


        grid2.add(therteen,0,0);
        grid2.add(fourteen,0,1);
        button3.setOnAction(event -> {class4.display("close","ARE YOU SURE YOU WANT TO CLOSE!!",stage);});
        button3.setFont(new Font(30));
        scene2=new Scene(grid2,600,700);
        stage.setScene(scene1);
        stage.setTitle("simple fitness tracker");
        stage.show();
    }

}
