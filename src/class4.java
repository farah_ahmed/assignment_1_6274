import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class class4 {
    public static void display(String title, String message,Stage stage) {
        Stage stage2 = new Stage();
        stage2.initModality(Modality.APPLICATION_MODAL);
        stage2.setTitle(title);
        stage2.setMinWidth(400);
        Label label = new Label(message);
        Button close = new Button("YES");
        close.setOnAction(event -> {
            stage.close();
            stage2.close();
        });
        VBox layout=new VBox(10);
        layout.getChildren().addAll(label,close);
        layout.setAlignment(Pos.CENTER);
        Scene scene=new Scene(layout);
        stage2.setScene(scene);
        stage2.showAndWait();
    }
}